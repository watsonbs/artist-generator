const colours = [
    "red",
    "green",
    "orange",
    "blue",
    "brown",
    "pink",
    "purple",
    "lightblue",
    "darkpurple"
];

export default colours;
