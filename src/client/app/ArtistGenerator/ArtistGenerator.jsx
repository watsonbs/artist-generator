import React from 'react';
import SessionSetUp from './SessionSetUp/SessionSetUp.jsx';
import SessionRunner from './SessionRunner/SessionRunner.jsx';

class ArtistGenerator extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};

    this.startSession = this.startSession.bind(this);
    this.endSession = this.endSession.bind(this);
    this.showModal = this.showModal.bind(this);
  }

  endSession() {
    this.setState({
      players: null,
      automatePhrases: null
    });
  }

  startSession (players, automatePhrases) {
    this.setState({
      players,
      automatePhrases
    });
  }

  showModal(modalName) {
    this.setState({
      openModal: modalName
    });
  }

  render() {
    return (

      <div className="artist-generator">
        <h1 className="ag-title">Artist Generator</h1>
        { (this.state.players && this.state.players.length) ? 
          <SessionRunner players={this.state.players} automatePhrases={this.state.automatePhrases} endSession={this.endSession} /> : 
          <SessionSetUp startSession={this.startSession} /> 
        }
        { this.state.openModal ? 
          <div className="ng-about-modal">
            <h2>About</h2>
            <p>A utility to help play <a href="https://boardgamegeek.com/boardgame/135779/fake-artist-goes-new-york">A Fake Artist Goes to New York</a></p>
            <p>Random word list taken from <a href="https://docs.google.com/spreadsheets/d/18xJWUHR3WnZCp7a7yyvnypQk3VWy0c67wqyiQMg-drE/edit#gid=0">here</a>.</p>
            <p><a className="ag-footer__link" onClick={() => this.showModal()}>Close</a></p>
          </div> :
          ''
        }
        <div className="ag-footer">
          <div className="ag-row ag-footer__content">
            <a className="ag-footer__link" onClick={() => this.showModal('about')}>About</a>
          </div>
        </div>
      </div>
    );
  }

}

export default ArtistGenerator;
