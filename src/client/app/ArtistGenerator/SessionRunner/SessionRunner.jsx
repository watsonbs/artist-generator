import React from 'react';
import RoundRunner from './RoundRunner/RoundRunner.jsx';
import RoundSetUp from './RoundSetUp/RoundSetUp.jsx';
import EndSession from './EndSession/EndSession.jsx';

class SessionRunner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            chooserIndex: 0,
            players: this.props.players
        };

        this.startRound = this.startRound.bind(this);
        this.endRound = this.endRound.bind(this);
        this.abortRound = this.abortRound.bind(this);
        this.endSession = this.endSession.bind(this);
        this.cancelSessionEnd = this.cancelSessionEnd.bind(this);
        this.confirmSessionEnd = this.confirmSessionEnd.bind(this);
    }

    endSession() {
        this.setState({
            showEndScreen: true
        });
    }
    cancelSessionEnd() {
        this.setState({
            showEndScreen: false
        });
    }
    confirmSessionEnd() {
        this.props.endSession();
    }
    
    endRound(isFakerWin, fakerIndex) {
        let players;
        if(isFakerWin) {
            players = this.state.players.map((player, index) => {
                if(
                    index === fakerIndex || 
                    (index === this.state.chooserIndex && !this.props.automatePhrases)
                ) {
                    const updatedPlayer = Object.assign({}, player);
                    updatedPlayer.score += 2;
                    
                    return updatedPlayer;
                }

                return player;
            });
        } else {
            players = this.state.players.map((player, index) => {
                if(
                    index === fakerIndex || 
                    (index === this.state.chooserIndex && !this.props.automatePhrases)
                ) {
                    return player;
                }
                
                const updatedPlayer = Object.assign({}, player);
                updatedPlayer.score++;
                
                return updatedPlayer;
            })
        }

        this.setState({
            players,
            chooserIndex: (this.state.chooserIndex + 1) % players.length,
            phrase: null,
            theme: null
        });
    }
    
    startRound(phrase, theme) {
        this.setState({
            phrase,
            theme
        });
    }

    abortRound() {
        this.setState({
            phrase: null,
            theme: null
        })
    }

    render() {
        const clueGiver = this.props.automatePhrases ? { name: 'Automated' } : this.props.players[this.state.chooserIndex % this.props.players.length];
        
        return (
            <div className="ag-content">
                <h2 className="ag-row">
                    { this.props.players.length } Players
                </h2>
                <h3 className="ag-subtitle ag-row">Clue giver: { clueGiver.name || clueGiver.colour }</h3>
                { this.state.phrase && this.state.theme ?  
                    <RoundRunner 
                        endRound={ this.endRound }
                        abortRound={ this.abortRound }
                        players={ this.state.players }
                        automatePhrases={ this.props.automatePhrases }
                        chooserIndex= { this.state.chooserIndex}
                        phrase={ this.state.phrase }
                        theme={ this.state.theme }
                    /> :
                    <RoundSetUp endSession={ this.endSession } startRound={ this.startRound } automatePhrases={ this.props.automatePhrases } players={ this.state.players }/>
                }
                { this.state.showEndScreen ? 
                    <EndSession players={this.state.players} confirm={this.confirmSessionEnd} cancel={this.cancelSessionEnd} />
                    : ''
                }
            </div>
        );
    }
}

export default SessionRunner;
