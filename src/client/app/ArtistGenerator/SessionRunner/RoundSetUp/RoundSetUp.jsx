import React from 'react';
import PhraseGenerator from './PhraseGenerator.js';
import PlayerScores from '../PlayerScores/PlayerScores.jsx';
import './round-set-up.scss';

class RoundSetUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            phrase: '',
            theme: ''
        };
        this.startRound = this.startRound.bind(this);
        this.handleInput = this.handleInput.bind(this);
        this.randomize = this.randomize.bind(this);
    }

    randomize() {
        return PhraseGenerator.getRandomPhrase().then((phrase) => {
            this.setState({
                theme: phrase.theme,
                phrase: phrase.phrase
            });
        });
    }
    
    startRound(event) {
        event.preventDefault();
        if(this.props.automatePhrases) {
            this.randomize().then(() => this.props.startRound(this.state.phrase, this.state.theme));
        }
        this.props.startRound(this.state.phrase, this.state.theme);
    }

    handleInput(event) {
        const stateChanges = {};
        stateChanges[event.target.name] = event.target.value;
        this.setState(stateChanges);
    }

    renderPhraseChooser() {
        return (
            <div>
                <div className="ag-row">
                    <button className="ag-button ag-button__secondary" type="button" onClick={ this.randomize }>Random</button>
                </div>
                <div className="ag-row">
                    <label className="ag-label">Theme</label>
                    <input className="ag-input" value={this.state.theme} name="theme" onChange={this.handleInput} />
                </div>
                <div className="ag-row">
                    <label className="ag-label">Phrase</label>
                    <input className="ag-input" value={this.state.phrase} name="phrase" onChange={this.handleInput} />
                </div>
            </div>
        );
    }

    render() {
        return (
            <div>
                <form onSubmit={ this.startRound }>
                    {
                        this.props.automatePhrases ? 
                        '' :
                        this.renderPhraseChooser()
                    }

                    <div className="ag-row">
                        <button className="ag-button" type="submit">Start</button>
                    </div>
                    <h3 className="ag-subtitle ag-row">Scores</h3>
                    <PlayerScores players={this.props.players} />
                </form>

                <div className="ag-row">
                    <button className="ag-button ag-button__secondary" onClick={this.props.endSession}>End Session</button>
                </div>
            </div>
        );
    }
}

export default RoundSetUp;
