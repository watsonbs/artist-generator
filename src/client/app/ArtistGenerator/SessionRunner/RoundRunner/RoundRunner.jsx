import React from 'react';
import './round-runner.scss';

const randBetween = (min, max) => {
    const result = Math.floor(Math.random() * max) + min;
    return result;
}

const colours = [
    'red',
    'green',
    'orange',
    'blue',
    'brown',
    'pink',
    'purple',
    'lightblue',
    'darkpurple'
]

class RoundRunner extends React.Component {
    constructor(props) {
        super(props);

        this.artistCount = this.props.automatePhrases ? this.props.players.length : this.props.players.length - 1;
        
        const currentIndex = (this.props.chooserIndex + 1) % this.props.players.length;
        const fakerOffset = this.props.automatePhrases ? 0 : currentIndex;
        const fakerIndex = randBetween(fakerOffset, fakerOffset + this.artistCount - 1) % this.props.players.length;

        this.state = {
            fakerIndex,
            currentIndex,
            playersDisplayed: 0,
            displayPhrase: false
        };

        this.continue = this.continue.bind(this);
        this.showPhrase = this.showPhrase.bind(this);
        this.artistWin = this.artistWin.bind(this);
        this.fakerWin = this.fakerWin.bind(this);
        this.renderPhraseDisplayer = this.renderPhraseDisplayer.bind(this);
    }

    fakerWin() {
        this.props.endRound(true, this.state.fakerIndex);
    }
    artistWin() {
        this.props.endRound(false, this.state.fakerIndex);
    }

    showPhrase() {
        this.setState({
            displayPhrase: true
        });
    }

    continue() {
        let nextIndex = (this.state.currentIndex+1) % this.props.players.length;
        
        this.setState({
            playersDisplayed: this.state.playersDisplayed +1,
            currentIndex: nextIndex,
            displayPhrase: false
        });
    }

    renderPhrase() {
        return (
            <div>
                { this.state.currentIndex === this.state.fakerIndex ? 
                    <span>You are the fake</span> :
                    <span>{ this.props.phrase }</span>
                }
            </div>
        );
    }

    renderPhraseDisplayer() {
        return (
            <div className={`ag-colour-block__${this.props.players[this.state.currentIndex].colour}`}>
                <div className="ag-reveal__name">{this.props.players[this.state.currentIndex].name || this.props.players[this.state.currentIndex].colour}</div>
                <div className="ag-reveal__control">
                    { this.state.displayPhrase ? 
                        this.renderPhrase() : ''
                    }

                    { this.state.displayPhrase ? 
                        <button className="ag-button" onClick={ this.continue }>Continue</button> :
                        <button className="ag-button" onClick={ this.showPhrase }>Show Phrase</button>
                    }
                </div>
            </div>
        )
    }

    renderRoundEnder() {
        return (
            <div>
                <div className="ag-row">
                    <button className="ag-button" onClick={ this.artistWin }>Artists Win</button>
                    <button className="ag-button ag-button__inline" onClick={ this.fakerWin }>Faker Wins</button>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div>
                <h3 className="ag-subtitle ag-row">Theme: { this.props.theme }</h3>
                { this.state.playersDisplayed < this.artistCount ?
                    this.renderPhraseDisplayer() :
                    this.renderRoundEnder()
                }
                <div className="ag-row">
                    <button className="ag-button ag-button__secondary" onClick={ this.props.abortRound }>Abort Round</button>
                </div>
            </div>
            
        );
    }
}

export default RoundRunner;
