import React from 'react';
import PlayerScores from '../PlayerScores/PlayerScores.jsx';
import './end-session.scss';


class EndSession extends React.Component {

    render() {
        
        return (
            <div className="ag-final-scores">
                <h2 className="ag-row ag-subtitle">Final Scores</h2>
                <PlayerScores players={this.props.players}/>
                <div className="ag-row">
                    <button className="ag-button ag-button__secondary" onClick={ this.props.cancel }>Cancel</button>
                    <button className="ag-button ag-button__inline" onClick={ this.props.confirm }>Confirm</button>
                </div>
            </div>
        );
    }

};

export default EndSession;
