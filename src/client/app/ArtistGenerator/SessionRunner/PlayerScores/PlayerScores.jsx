import React from 'react';


class PlayerScores extends React.Component {

    render() {
        const players = [ ...this.props.players ].sort((now, next) => next.score - now.score);
            
        const playerElements = players.map((player, index) => {
            return (
                <div key={`player-${index}`} className="ag-row">
                    <div className={`ag-small-square ag-colour-block__${player.colour}`}>{ player.score }</div>
                    <div className="ag-player-list__player">{ player.name || player.colour }</div>
                </div>
            );
        })
    
        return (
            <div className="ag-score-list">
                { playerElements }
            </div>
        );
    }

};

export default PlayerScores;
