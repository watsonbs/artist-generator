import React from 'react';

class ColourPickerButton extends React.Component {
    constructor(props) {
        super(props);
        
        this.changeColour = this.changeColour.bind(this);
    }

    changeColour () {
        this.props.changeColour();
    }

    render() {
        return (
            <div 
                onClick={this.changeColour} 
                className={`ag__clickable ag-small-square ag-colour-block__${this.props.colour}`}
            ></div>
        )
    }
}
export default ColourPickerButton;
