import React from 'react';
import colours from '../../colours.js';
import './colour-picker-modal.scss';

class ColourPickerModal extends React.Component {
    constructor(props) {
        super(props);
        
        this.pickColour = this.pickColour.bind(this);
    }

    pickColour (colour) {
        this.props.pickColour(colour);
    }

    render() {
        const reoganisedColours = colours.filter(colour => colour !== this.props.currentColour);
        reoganisedColours.push(this.props.currentColour);

        return (
            <div className="ag-colour-grid">
                { reoganisedColours.map(colour => 
                    <div 
                        key={`colour-selection-${colour}`}
                        className={`ag__clickable ag-small-square ag-colour-block__${colour}`}
                        onClick={() => this.pickColour(colour)}
                    ></div>
                )}
            </div>
        )
    }
}
export default ColourPickerModal;
