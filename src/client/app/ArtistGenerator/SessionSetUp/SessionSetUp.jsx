import React from 'react';
import ColourPickerButton from './ColourPickerButton/ColourPickerButton.jsx';
import ColourPickerModal from './ColourPickerModal/ColourPickerModal.jsx';
import colours from '../colours.js';

class SessionSetUp extends React.Component {

    constructor(props) {
        super(props);

        const players = [];
        this.state = {
            players,
            automatePhrases: false
        };
        this.state.players.push(this.createPlayer());
        this.state.players.push(this.createPlayer());
        this.state.players.push(this.createPlayer());
        this.state.players.push(this.createPlayer());

        this.startSession = this.startSession.bind(this);
        this.addPlayer = this.addPlayer.bind(this);
        this.removePlayer = this.removePlayer.bind(this);
        this.updatePlayerName = this.updatePlayerName.bind(this);
        this.updatePlayerColour = this.updatePlayerColour.bind(this);
        this.toggleAutomatePhrases = this.toggleAutomatePhrases.bind(this);
        this.openColourPicker = this.openColourPicker.bind(this);
        this.closeColourPicker = this.closeColourPicker.bind(this);
    }

    startSession () {
        this.props.startSession(this.state.players, this.state.automatePhrases);
    }

    createPlayer(index) {
        const availableColours = colours.filter(colour => !this.state.players.find(player => player.colour === colour));
        return {
            name: '',
            colour: availableColours[0],
            score: 0
        };
    }

    addPlayer () {
        const players = [ ...this.state.players, this.createPlayer(this.state.players.length)];
        this.setState({
            players
        });
    }

    removePlayer () {
        const players = [ ...this.state.players ];
        players.pop();
        this.setState({
            players
        });
    }

    updatePlayerName (index, event) {
        const players = [ ...this.state.players ];
        const player = Object.assign({}, this.state.players[index]);
        players[index] = player;
        player.name = event.target.value;
        
        this.setState({
            players
        });
    }

    closeColourPicker (event) {
        this.setState({
            openModal: false
        });
        document.removeEventListener('click', this.closeColourPicker);

    }
    openColourPicker (index) {
        document.addEventListener('click', this.closeColourPicker);

        this.setState({
            openModal: `colour-picker-${index}`
        });
    }

    updatePlayerColour (index, newColour) {
        const players = [ ...this.state.players ];
        const player = Object.assign({}, this.state.players[index]);

        const previousColourOwnerIndex = players.findIndex(player => player.colour === newColour);
        if(previousColourOwnerIndex > -1) {
            const oldColour = player.colour;
            const previousColourOwner = Object.assign({}, players[previousColourOwnerIndex]);
            previousColourOwner.colour = oldColour;
            players[previousColourOwnerIndex] = previousColourOwner;
        }


        player.colour = newColour;
        players[index] = player;

        this.setState({
            players,
            openModal: false
        });
    }

    toggleAutomatePhrases () {
        this.setState({
            automatePhrases: !this.state.automatePhrases
        });
    }

    renderPlayers () {
        const playerElements = this.state.players.map((player, index) => (
            <li key={`player-${index}`} className="ag-input-list__item">
                <div className="ag-input-list__content">
                    <input 
                        className="ag-input"
                        placeholder={player.colour}
                        name={`player-name-${index}`}
                        type="text"
                        onChange={(event) => this.updatePlayerName(index, event)}
                    />
                    <ColourPickerButton changeColour={() => this.openColourPicker(index)} colour={player.colour} />
                    { this.state.openModal === `colour-picker-${index}` ? 
                        <ColourPickerModal 
                            currentColour={player.colour} 
                            pickColour={
                                (colour) => {
                                    this.updatePlayerColour(index, colour);
                                }
                            } 
                        /> : ''
                    }
                </div>
            </li>
        ));
        return (
            <ol className="ag-input-list">
                { playerElements }
            </ol>
        );
    }

    render() {
        const maxPlayersReached = this.state.players.length === colours.length;
        const minPlayersReached = this.state.players.length === 4;
        return (
            <div className="ag-content">
                <h2 className="ag-subtitle">Players</h2>
                <div className="ag-row">
                    { this.renderPlayers() }
                </div>
                <div className="ag-row">
                    <button
                        className={`ag-button ag-button__secondary ${minPlayersReached ? 'ag-button__disabled' : ''}`} 
                        disabled={minPlayersReached}
                        onClick={this.removePlayer}
                    >
                        Remove player
                    </button>
                    <button 
                        className={`ag-button ag-button__inline ${maxPlayersReached ? 'ag-button__disabled' : ''}`} 
                        disabled={maxPlayersReached}
                        onClick={this.addPlayer}
                    >
                        Add player
                    </button>
                </div>
                <div className="ag-row">
                    <span className="ag-checkbox__container">
                        <input className="ag-checkbox__input" id="automatePhrases" type="checkbox" value={this.state.automatePhrases} onChange={this.toggleAutomatePhrases} />
                        <label className="ag-checkbox" htmlFor="automatePhrases"></label>
                    </span>
                    <label className="ag-label" htmlFor="automatePhrases">Automate Clues</label>
                </div>
                
                <div className="ag-row"><button className="ag-button" onClick={this.startSession}>Start</button></div>
            </div>
        );
    }

}

export default SessionSetUp;
