import React from 'react';
import {render} from 'react-dom';
import ArtistGenerator from './ArtistGenerator/ArtistGenerator.jsx';
import './styles/styles.scss';

class App extends React.Component {
  render () {
    return (
      <ArtistGenerator />
    );
  }
}

render(<App/>, document.getElementById('app'));
